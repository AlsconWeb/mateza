<?php

namespace luckywp\scriptsControl\front;

use luckywp\scriptsControl\core\base\BaseObject;
use luckywp\scriptsControl\core\Core;
use luckywp\scriptsControl\core\helpers\ArrayHelper;
use luckywp\scriptsControl\plugin\entities\Area;

class Front extends BaseObject
{

    public function init()
    {
        $itemsByArea = Core::$plugin->items->findAllGroupedByArea(true);

        if ($itemsByArea[Area::HEAD]) {
            add_action('wp_head', function () use ($itemsByArea) {
                echo implode("\n", ArrayHelper::getColumn($itemsByArea[Area::HEAD], 'body'));
            });
        }

        if ($itemsByArea[Area::BODY_BEGIN]) {
            add_action('template_include', function ($template) use ($itemsByArea) {
                ob_start(function ($buffer) use ($itemsByArea) {
                    return preg_replace('/<body[^>]*>/imu', '$0' . implode("\n", ArrayHelper::getColumn($itemsByArea[Area::BODY_BEGIN], 'body')), $buffer, 1);
                });
                return $template;
            }, 999);
        }

        if ($itemsByArea[Area::BODY_END]) {
            add_action('wp_footer', function () use ($itemsByArea) {
                echo implode("\n", ArrayHelper::getColumn($itemsByArea[Area::BODY_END], 'body'));
            });
        }
    }
}
