<?php
return [
    'textDomain' => 'lwpsc',
    'bootstrap' => [
        'admin',
        'front',
    ],
    'pluginsLoadedBootstrap' => [],
    'components' => [
        'admin' => \luckywp\scriptsControl\admin\Admin::className(),
        'front' => \luckywp\scriptsControl\front\Front::className(),
        'items' => \luckywp\scriptsControl\plugin\repositories\ItemRepository::className(),
        'options' => \luckywp\scriptsControl\core\wp\Options::className(),
        'request' => \luckywp\scriptsControl\core\base\Request::className(),
        'view' => \luckywp\scriptsControl\core\base\View::className(),
    ],
];
