msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2018-10-09 09:40+0300\n"
"PO-Revision-Date: 2018-10-09 09:40+0300\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.1.1\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: _;_html;_attr\n"
"X-Poedit-SearchPath-0: .\n"

#: admin/Admin.php:30 admin/Admin.php:43 admin/Admin.php:44
#: admin/views/scripts/index.php:14
msgid "Scripts"
msgstr ""

#: admin/controllers/ItemController.php:145
msgid "Code Deleted"
msgstr ""

#: admin/controllers/ItemController.php:145
msgid "Hide"
msgstr ""

#: admin/forms/item/ItemForm.php:42
msgid "Caption"
msgstr ""

#: admin/forms/item/ItemForm.php:43
msgid "Body"
msgstr ""

#: admin/views/item/_modal_add.php:14 admin/views/item/_modal_add.php:53
#: admin/views/item/_modal_delete_confirm.php:13
#: admin/views/item/_modal_delete_confirm.php:35
#: admin/views/item/_modal_edit.php:14 admin/views/item/_modal_edit.php:53
msgid "Cancel"
msgstr ""

#: admin/views/item/_modal_add.php:15
msgid "New Code"
msgstr ""

#: admin/views/item/_modal_add.php:56
msgid "Add"
msgstr ""

#: admin/views/item/_modal_delete_confirm.php:14
msgid "Confirmation"
msgstr ""

#: admin/views/item/_modal_delete_confirm.php:29
msgid "Are you sure to delete this code?"
msgstr ""

#: admin/views/item/_modal_delete_confirm.php:38
#: admin/widgets/itemRow/views/row.php:25
msgid "Delete"
msgstr ""

#: admin/views/item/_modal_edit.php:15
msgid "Edit Code"
msgstr ""

#: admin/views/item/_modal_edit.php:56
msgid "Save"
msgstr ""

#: admin/views/scripts/index.php:30 admin/views/scripts/index.php:47
#: admin/views/scripts/index.php:62
msgid "Add Code"
msgstr ""

#: admin/views/scripts/index.php:51
msgid "page content"
msgstr ""

#: admin/widgets/itemRow/views/row.php:20
msgid "Disable"
msgstr ""

#: admin/widgets/itemRow/views/row.php:22
msgid "Enable"
msgstr ""

#: admin/widgets/itemRow/views/row.php:24
msgid "Edit"
msgstr ""

#: core/admin/AdminController.php:51
msgid "Sorry, you are not allowed to access this page."
msgstr ""

#: core/validators/RequiredValidator.php:19
msgid "{attribute} cannot be blank."
msgstr ""

#: plugin/Plugin.php:24
msgid ""
"A great way to insert and manage custom code (CSS, JS, meta tags, etc.) into "
"website before &lt;/head&gt;, after &lt;body&gt; or before &lt;/body&gt;."
msgstr ""