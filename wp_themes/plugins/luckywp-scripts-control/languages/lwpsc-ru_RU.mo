��          �      |      �  �   �     �     �  !   �     �     �     �     �     �     �     �     �  	   �          
                 /   %     U     b  �    �   }     x     �  H   �     �     �               3     N     ]     p  !   �     �     �     �     �     �  d   �  %   d  8   �                                                                                
       	          A great way to insert and manage custom code (CSS, JS, meta tags, etc.) into website before &lt;/head&gt;, after &lt;body&gt; or before &lt;/body&gt;. Add Add Code Are you sure to delete this code? Body Cancel Caption Code Deleted Confirmation Delete Disable Edit Edit Code Enable Hide New Code Save Scripts Sorry, you are not allowed to access this page. page content {attribute} cannot be blank. Project-Id-Version: 
POT-Creation-Date: 2018-10-10 08:59+0300
PO-Revision-Date: 2018-10-10 09:00+0300
Last-Translator: 
Language-Team: 
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.1.1
X-Poedit-Basepath: ..
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;esc_html__;esc_attr__
X-Poedit-SearchPath-0: .
 Отличный способ для вставки и управления произвольным кодом (CSS, JS, мета-теги и т. д.) на сайте перед &lt;/head&gt;, после &lt;body&gt; или перед &lt;/body&gt;. Добавить Добавить код Вы уверены, что хотите удалить этот код? Содержимое Отмена Заголовок Код удалён Подтверждение Удалить Отключить Редактировать Редактировать код Включить Скрыть Новый код Сохранить Скрипты Извините, вам не разрешено просматривать эту страницу. содержимое страницы Необходимо заполнить «{attribute}». 