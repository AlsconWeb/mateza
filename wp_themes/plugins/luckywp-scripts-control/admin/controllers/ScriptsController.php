<?php

namespace luckywp\scriptsControl\admin\controllers;

use luckywp\scriptsControl\core\admin\AdminController;
use luckywp\scriptsControl\core\Core;
use ReflectionException;

class ScriptsController extends AdminController
{

    /**
     * @throws ReflectionException
     */
    public function actionIndex()
    {
        $this->render('index', [
            'itemsByArea' => Core::$plugin->items->findAllGroupedByArea(),
        ]);
    }
}
