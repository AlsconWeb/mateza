<?php 
/**
 * Enqueue child styles
 */
add_action( 'wp_enqueue_scripts', 'enqueue_child_theme_styles', 100);

function enqueue_child_theme_styles() {
	wp_enqueue_style( 'root-style-child', get_stylesheet_uri(), array( 'main-style' )  );
}