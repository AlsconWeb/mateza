<?php
/**
 * Template Name: Яндекс Маркет
 * Author: Alex L. 
 * Author UTL: https://www.upwork.com/freelancers/~015d44c720e5f4462a
 * Author UTL: https://freelance.ru/lovik
 */

 $curs = get_field('rub_to_usd', 'options');
 $delivery = get_field('delivery', 'options');
 $args = array(
    'post_type' => 'product',
    'posts_per_page' =>-1,
);

$query = new WP_Query($args);
$products = $query->posts;

 $termsArr = get_terms( array(
    'taxonomy'      => array( 'product-cat'), // название таксономии с WP 4.5
    'orderby'       => 'id', 
    'order'         => 'ASC',
    'hide_empty'    => true, 
    'object_ids'    => null,
    'include'       => array(),
    'exclude'       => array(), 
    'exclude_tree'  => array(), 
    'fields'        => 'all', 
    'count'         => false,
    'hierarchical'  => true, 
    'child_of'      => 0, 
    'pad_counts'    => false, 
    'cache_domain'  => 'core',
    'childless'     => false, // true не получит (пропустит) термины у которых есть дочерние термины. C 4.2.
    'update_term_meta_cache' => true, // подгружать метаданные в кэш
) );
 ?>
<?php echo '<?xml version="1.0" encoding="utf-8"?>'; ?>
<yml_catalog date="<?php echo current_time('mysql');?>">
    <shop>
        <name><?php bloginfo( 'name' );?></name>
        <company><?php bloginfo( 'name' );?></company>
        <url><?php bloginfo( 'url' );?></url>

        <currencies>
            <currency id="RUR" rate="<?php echo  $curs['usd']?>" />
            <currency id="USD" rate="<?php echo  $curs['rub']?>" />
        </currencies>

        <categories>
            <?php foreach($termsArr as $item):?>
            <category id="<?php echo $item->term_id; ?>"><?php echo $item->name;?></category>
            <?php endforeach;?>
        </categories>

        <delivery-options>
            <option cost="<?php echo $delivery['cost_delivery'];?>" days="<?php echo $delivery['delivery_time'];?>" />
        </delivery-options>
        <?php #var_dump($products);?>
        <offers>
            <?php foreach($products as $product):?>
            <?php 
                // get ACF field in post
                // var_dump($product);
                $id = $product->ID;

                $title = get_field('title_product_YM',$id);
                $colors = get_field('colors', $id);
                $modelPhoto = get_field('model_image',$id);
                $size = get_field('size_and_price', $id);
                $catID = get_the_terms( $product->ID, 'product-cat');
                $description  = get_field('desctiption_product_YM', $id);
                $material = get_field('material', $id);
            ?>

            <?php foreach($colors as $key => $color):?>
            <?php 
                $image = $color["sizes"][0]["images"]; 
                $colorName = $colors[$key]["color"]["name_color"];
                $colorSize =$color["sizes"];
                // var_dump($image);
            ?>
            <?php foreach($colorSize as $j => $item):?>
            <offer id="<?php echo count($colors)."$id".rand (1, 1000 );?>" available="true">
                <url><?php echo $product->guid;?></url>
                <price><?php echo preg_replace("/\s+/", "", $colorSize[$j]["param_size"]['price']);?>
                </price>
                <currencyId>RUR</currencyId>
                <categoryId><?php echo $catID[0]->term_id; ?></categoryId>
                <?php foreach($image as $img):?>
                <picture><?php echo $img["image"];?></picture>
                <?php endforeach;?>
                <delivery>true</delivery>
                <name><?php echo $title;?></name>
                <vendor><?php echo $product->post_title;?></vendor>
                <model><?php echo $product->post_title;?></model>
                <description><?php echo $description;?></description>
                <manufacturer_warranty>true</manufacturer_warranty>
                <param name="Цвет"><?php echo $colorName;?></param>
                <param name="Материал"><?php echo $material;?></param>
                <param name="Размер"><?php echo $item["param_size"]['size']; ?></param>
                <param name="Вес"><?php echo $item["specifications"]["wt"];?></param>
                <param name="Высота"><?php echo $item["specifications"]["height"];?></param>
                <param name="Глубина"><?php echo $item["specifications"]["depth"];?></param>
                <param name="Объем"><?php echo $item["specifications"]["vol"];?></param>
                <param name="Ширина"><?php echo $item["specifications"]["width"];?></param>
            </offer>
            <?php endforeach;?>
            <?php endforeach;?>
            <?php endforeach;?>
        </offers>
    </shop>
</yml_catalog>