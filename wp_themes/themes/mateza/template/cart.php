<?php 
/**
 * Template Name: Корзина
 * Author: Alex L. 
 * Author UTL: https://www.upwork.com/freelancers/~015d44c720e5f4462a
 * Author UTL: https://freelance.ru/lovik
 */

session_start();

$cartArray =  array_map("unserialize", array_unique(array_map("serialize", $_SESSION['newOrder'])));

// var_dump($cartArray);

$_SESSION['newOrder'] = $cartArray;
// $_SESSION['newOrder'] = "";

 //block baggage
$title_baggage = get_field('title_baggage', $id);
$text_baggage = get_field('text_baggage', $id);
$list_baggage = get_field('list_baggage', $id);
$btn_baggage_text = get_field('btn_baggage_text', $id);
$btn_baggage_link = get_field('btn_baggage_link', $id);

//block delivery
$b2_title = get_field('b2_title', $id);
$b2_left_list = get_field('b2_left_list', $id);
$b2_right_list = get_field('b2_right_list', $id);
$b2_btn_more_tetx = get_field('b2_btn_more_tetx', $id);
$b2_btn_more_link = get_field('b2_btn_more_link', $id);
$b2_btn_modal_tetx = get_field('b2_btn_modal_tetx', $id);
$b2_btn_modal_link = get_field('b2_btn_modal_link', $id);

//block payment
$b3_title = get_field('b3_title', $id);
$b3_list = get_field('b3_list', $id);
$b3_btn_more_tetx = get_field('b3_btn_more_tetx_copy', $id);
$b3_btn_more_link = get_field('b3_btn_more_link', $id);

//block modal 
$modal_image = get_field('modal_image', $id);
$modal_title = get_field('modal_title', $id);
$modal_form_text = get_field('modal_form_text', $id);

?>
<?php get_header();?>
<section>
    <div class="cart-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h1>Ваша корзина</h1>
                    <div class="product">
                        <div class="items">
                            <?php foreach($cartArray as $key => $item):?>
                            <div class="item" data-index="<?php echo $key; ?>">
                                <div class="image"><img src="<?php echo $item["image"];?>" width="100%" height="auto"></div>
                                <div class="name">
                                    <p><?php echo $item["model"];?></p>
                                    <div class="tags">
                                        <div class="tag">Цвет - <?php echo $item["color"];?></div>
                                        <div class="tag">Размер - <?php echo $item["size"];?></div>
                                    </div>
                                </div>
                                <div class="quantity">
                                    <p>Количество</p>
                                    <div class="input-quantity"><span class="icon-minus input-number-decrement"></span>
                                        <input class="input-number" type="text" value="1" min="0" max="999"><span
                                            class="icon-plus input-number-increment"></span>
                                    </div>
                                </div>
                                <div class="price"><?php echo $item["price"];?><i class="icon-ruble"></i></div>
                                <div class="actions">
                                    <div class="icon-remove"></div>
                                </div>
                                <input type="hidden" name="price" value="<?php echo $item["price"];?>">
                            </div>
                            <?php endforeach;?>
                        </div>
                        <div class="total">
                            <p class="price">Итого:<strong>2 390,00</strong><i class="icon-ruble"></i></p><a class="button icon-cart to-checkout"
                                href="#">Перейти
                                к оформлению заказа</a>
                        </div>
                    </div>
                    <div class="desc">
                        <h2><?php echo $title_baggage?></h2>
                        <div class="items">
                            <div class="item">
                                <p>
                                    <?php $text_baggage;?>
                                </p>
                                <ol>
                                    <?php foreach($list_baggage as $list):?>
                                    <li><?php echo $list['text'];?></li>
                                    <?php endforeach;?>
                                </ol>
                            </div>
                        </div>
                        <ul class="buttons">
                            <li><a class="button" href="<?php echo $btn_baggage_link; ?>"><?php echo $btn_baggage_text; ?><i
                                        class="icon-up-arrow"></i></a></li>
                        </ul>
                    </div>
                    <div class="desc">
                        <h2><?php echo $b3_title; ?></h2>
                        <div class="items">
                            <div class="item">
                                <p class="title"><?php echo $b2_left_list['title'];?></p>
                                <p><?php echo $b2_left_list['subtitle'];?></p>
                                <ul>
                                    <?php foreach($b2_left_list['list'] as $list):?>
                                    <li><?php echo $list['text'];?></li>
                                    <?php endforeach;?>
                                </ul>
                            </div>
                            <div class="item">
                                <p class="title"><?php echo $b2_right_list['title'];?></p>
                                <p><strong><?php echo $b2_right_list['subtitle'];?></strong></p>
                                <ul>
                                    <?php foreach($b2_right_list['list'] as $list):?>
                                    <li><?php echo $list['text'];?></li>
                                    <?php endforeach;?>
                                </ul>
                            </div>
                        </div>
                        <ul class="buttons">
                            <li><a class="button" href="<?php echo $b2_btn_more_link;?>"><?php echo $b2_btn_more_tetx;?><i
                                        class="icon-up-arrow"></i></a></li>
                            <li><a class="button" href="<?php echo $b2_btn_modal_link;?>" data-toggle="modal"
                                    data-target=".calculate"><?php echo $b2_btn_modal_tetx;?><i class="icon-suitcase"></i></a></li>
                        </ul>
                    </div>
                    <div class="desc">
                        <h2><?php echo $b2_title;?></h2>
                        <div class="items">
                            <?php foreach($b3_list as $list):?>
                            <div class="item">
                                <p><strong><?php echo $list['title'];?></strong></p>
                                <p><?php echo $list['text'];?></p>
                            </div>
                            <?php endforeach;?>
                        </div>
                        <ul class="buttons">
                            <li><a class="button" href="<?php echo $b3_btn_more_link;?>"><?php echo $b3_btn_more_tetx;?><i
                                        class="icon-up-arrow"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade calculate" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"><a class="close icon-remove" href="#" data-dismiss="modal" aria-hidden="true"></a>
            <div class="left-block"><img src="<?php echo $modal_image['url'];?>" alt="<?php echo $modal_image['title'];?>"></div>
            <div class="right-block">
                <form method="post" class="forms-contact" data-init="cart">
                    <p><?php echo $modal_title;?></p>
                    <div class="input">
                        <label>Введите имя</label>
                        <input type="text" placeholder="Иван Иванов" name="name" required>
                    </div>
                    <div class="input">
                        <label>Введите e-mail</label>
                        <input type="email" placeholder="example@mail.com" name="email" required>
                    </div>
                    <div class="input">
                        <label>Введите телефон</label>
                        <input type="tel" placeholder="+7 (495) 000 - 00 - 00" name="phone" required>
                    </div>
                    <div class="input">
                        <label>Введите точный адрес доставки</label>
                        <input type="text" placeholder="101000, г. Москва, ул. Кравченко, д.123, кв.1," name="address">
                    </div>
                    <div class="input">
                        <input type="submit" value="Рассчитать">
                    </div>
                    <p><?php echo $modal_form_text;?></p>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
var totalItemsOrder = 0
totalItemsOrder = <?php 
if($_SESSION['newOrder']){
    echo json_encode($_SESSION['newOrder']);
}else {
     echo "0"; 
    }
?>
</script>
<?php get_footer( );?>