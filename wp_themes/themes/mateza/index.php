<?php 
/**
 * Template Name: Home
 * Author: Alex L. 
 * Author UTL: https://www.upwork.com/freelancers/~015d44c720e5f4462a
 * Author UTL: https://freelance.ru/lovik
 */

 $id = $post->ID;
 // block card product
 $b1_title = get_field('b1_title', $id);
 
 // block Benefits
 $b2_title = get_field('b2_title', $id);
 $b2_prem = get_field('b2_prem', $id);

 //block Promotions
 $b3_title = get_field('b3_title', $id);
 $b3_bg = get_field('b3_bg', $id);
 $b3_text = get_field('b3_text', $id);
 $b3_image = get_field('b3_image', $id);

 // block Discounts
 $b4_bg = get_field('b4_bg', $id);
 $b4_title = get_field('b4_title', $id);
 $b4_text = get_field('b4_text', $id);
 $b4_btn_text = get_field('b4_btn_text', $id);
 $b4_btn_url = get_field('b4_btn_url', $id);
 $b4_sales_product = get_field('b4_sales_product', $id);

 // block Discounts slider
 $b5_slider = get_field('b5_slider', $id);

 //block Guarantees
 $b6_title = get_field('b6_title', $id);
 $b6_subtitle = get_field('b6_subtitle', $id);
 $guarantees = get_field('guarantees', $id);

 // block Technology
 $b7_title = get_field('b7_title', $id);
 $b7_sub_title = get_field('b7_sub_title', $id);
 $b7_left_part = get_field('b7_left_part', $id);
 $b7_canter_image = get_field('b7_canter_image', $id);
 $b7_right_part = get_field('b7_right_part', $id);

 // block Video
 $b8_title = get_field('b8_title', $id);
 $b8_video = get_field('b8_video', $id);

 // block portfolio 
 $b9_title = get_field('b9_title', $id);
 $b9_sub_title = get_field('b9_sub_title', $id);
 $b9_slider = get_field('b9_slider', $id);

 // block Reviews
 $b10_title = get_field('b10_title', $id);
 $b10_subtitle = get_field('b10_subtitle', $id);
 $b10_slider = get_field('b10_slider', $id);
 $b10_bnt_text = get_field('b10_bnt_text', $id);
 $b10_bnt_link = get_field('b10_bnt_link', $id);

 //block about us
 $b11_title = get_field('b11_title', $id);
 $b11_image_title = get_field('b11_image_title', $id);
 $b11_image = get_field('b11_image', $id);
 $b11_imege_text = get_field('b11_imege_text', $id);
 $b11_text = get_field('b11_text', $id);

 // block partners 
 $b12_title = get_field('b12_title', $id);
 $b12_logos = get_field('b12_logos', $id);

 // block partners
 $b13_title = get_field('b13_title', $id);
 $b12_logos_payment = get_field('b12_logos_payment', $id);


?>

<?php get_header();?>
<div class="catalog">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h1><?php echo $b1_title;?></h1>
                <div id=app></div>
            </div>
        </div>
    </div>
</div>
<div class="advantages">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2><?php echo $b2_title;?></h2>
                <div class="items">
                    <?php foreach($b2_prem as $item): ?>
                    <div class="item">
                        <p><?php echo $item['text_description'];?></p>
                        <img class="lazy" data-src="<?php echo $item['icon']['url']; ?>" alt="<?php echo $item['icon']['title']; ?>">
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="banner-title">
    <h2><?php echo $b3_title;?></h2>
    <div class="banner-mini shares lazy" data-src="<?php echo $b3_bg['url'];?>">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="left-block">
                        <p><?php echo $b3_text;?></p>
                    </div>
                    <div class="right-block"><img class="lazy" data-src="<?php echo $b3_image['url'];?>" alt="<?php echo $b3_image['title'];?>"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if($b4_bg['url']):?>
<div class="discounts lazy" data-src="<?php echo $b4_bg['url'];?>">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="description">
                    <h2><?php echo $b4_title;?></h2>
                    <?php echo $b4_text;?>
                    <a class="button icon-phone" href="#" data-toggle="modal" data-target=".modal-sales"><?php echo $b4_btn_text;?></a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif;?>
<div class="shares">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="slider-shares">
                    <?php foreach($b5_slider as $key => $slide):?>
                    <div class="item">
                        <img class="lazy" data-src="<?php echo $slide["slide"]["bg"]['url'];?>" alt="<?php echo $slide["bg"]['title'];?>">
                        <div class="description">
                            <p><?php echo $slide["slide"]["type"];?></p>
                            <?php echo $slide["slide"]["title"];?>
                            <a class="button icon-phone" href="<?php echo $slide["slide"]["btn_link"];?>" data-toggle="modal"
                                data-target=".modal-<?php echo $key;?>"><?php echo $slide["slide"]["btn_text"];?></a>
                            <p><?php echo $slide["slide"]["text_down_btn"];?></p>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="warranty">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2><?php echo $b6_title;?> <span><?php echo $b6_subtitle;?></span></h2>
                <div class="items">
                    <?php foreach($guarantees as $item):?>
                    <div class="item">
                        <p><?php echo $item['guarantee']['title'];?></p>
                        <?php if($item['guarantee']['btn_link']):?>
                        <div class="img lazy" data-src="<?php echo $item['guarantee']['image']['url'];?>">
                            <a class="button" href="#" data-toggle="modal" data-target=".modal-form"><?php echo $item['guarantee']['btn_text'];?></a>
                        </div>
                        <?php else:?>
                        <div class="img lazy" data-src="<?php echo $item['guarantee']['image']['url'];?>">
                            <a class="button" href="<?php echo $item['guarantee']['image']['url'];?>"
                                data-fancybox><?php echo $item['guarantee']['btn_text'];?></a>
                        </div>
                        <?php endif;?>
                        <p><?php echo $item['guarantee']['text'];?></p>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="technology">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2><?php echo $b7_title;?></h2>
                <p><?php echo $b7_sub_title;?></p>
                <div class="items">
                    <div class="item">
                        <ul>
                            <?php foreach($b7_left_part as $item):?>
                            <li>
                                <img class="lazy" data-src="<?php echo $item['image']['url'];?>" alt="<?php echo $item['image']['title'];?>">
                                <p><?php echo $item['title'];?></p>
                                <p><?php echo $item['text'];?></p>
                            </li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                    <div class="item"><img class="lazy" data-src="<?php echo $b7_canter_image['url'];?>" alt="<?php echo $b7_canter_image['url'];?>">
                    </div>
                    <div class="item">
                        <ul>
                            <?php foreach($b7_right_part as $item):?>
                            <li>
                                <img class="lazy" data-src="<?php echo $item['image']['url'];?>" alt="<?php echo $item['image']['title'];?>">
                                <p><?php echo $item['title'];?></p>
                                <p><?php echo $item['text'];?></p>
                            </li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="video-block">
                    <h2><?php echo $b8_title;?></h2>
                    <div class="video icon-play">
                        <video src="<?php echo $b8_video['url'];?>" poster="<?php bloginfo( 'template_url' )?>/img/video.jpg"></video>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="portfolio">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2><span><?php echo $b9_sub_title;?></span><?php echo $b9_title;?></h2>
                <div class="portfolio-slider">
                    <?php foreach ($b9_slider as $item):?>
                    <div class="item">
                        <img class="lazy" data-src="<?php echo $item['image']['url'];?>" alt="<?php echo $item['image']['title'];?>">
                        <a data-fancybox="gallery-5" href="<?php echo $item['image']['url'];?>"></a>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="reviews">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2><?php echo $b10_title;?> <span><?php echo $b10_subtitle;?></span></h2>
                <div class="reviews-slider">
                    <?php foreach($b10_slider as $item):?>
                    <div class="item">
                        <img class="lazy" data-src="<?php echo $item['image']['url'];?>" alt="<?php echo $item['image']['title'];?>">
                        <a data-fancybox="gallery-6" href="<?php echo $item['image']['url'];?>"></a>
                    </div>
                    <?php endforeach;?>
                </div><a class="button" href="<?php echo $b10_bnt_link;?>"><?php echo $b10_bnt_text;?></a>
            </div>
        </div>
    </div>
</div>
<div class="about-company">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2><strong><?php echo $b11_title;?></strong></h2>
                <div class="left-block">
                    <p><?php echo $b11_image_title;?></p>
                    <img class="lazy" data-src="<?php echo $b11_image['url'];?>" alt="<?php echo $b11_image['title'];?>">
                    <p><?php echo $b11_imege_text;?></p>
                </div>
                <div class="right-block">
                    <?php echo $b11_text;?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="partners">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2><?php echo $b12_title;?></h2>
                <div class="items">
                    <?php foreach($b12_logos as $item):?>
                    <div class="item">
                        <a href="#"><img class="lazy" data-src="<?php echo $item['logo']['url']?>" alt="<?php echo $item['logo']['title']?>"></a>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="cards">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2><strong><?php echo $b13_title;?></strong></h2>
                <ul class="card">
                    <?php foreach($b12_logos_payment as $item):?>
                    <li><img class="lazy" data-src="<?php echo $item['logo']['url'];?>" alt="<?php echo $item['logo']['title'];?>"></li>
                    <?php endforeach;?>
                </ul>
            </div>
        </div>
    </div>
</div>
</section>
<!-- modals sales block (b4) -->
<?php 

$id = $b4_sales_product->ID;
$product_title = get_the_title($id);
$color = get_field('colors', $id);
if(get_field('colors', $id)){
    $image = $color[0]['color']['photo_color_product'][0]['image'];
}
else{
   $model = get_field('model_image', $id);
   $image = $model[0]['image'];
}

$size = get_field('size_and_price', $id);


?>


<div class="modal fade checkout-modal  modal-sales" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"><a class="close icon-remove" href="#" data-dismiss="modal" aria-hidden="true"></a>
            <form class="checkout-forms-modal">
                <div class="left-block">
                    <p>Оформление заказа<span>Настоящим подтверждаю, что я ознакомлен и согласен с условиями<a>политики конфиденциальности</a></span>
                    </p>
                    <div class="input">
                        <label>Введите имя</label>
                        <input type="text" placeholder="Иван Иванов" name="name">
                    </div>
                    <div class="input">
                        <label>Введите e-mail</label>
                        <input type="email" placeholder="example@mail.com" name="email">
                    </div>
                    <div class="input">
                        <label>Введите телефон</label>
                        <input type="tel" placeholder="+7 (495) 000 - 00 - 00" name="phone">
                    </div>
                    <div class="textarea">
                        <label>Комментарии</label>
                        <textarea placeholder="Пожелания..." name="massage"></textarea>
                    </div>
                    <div class="radio-buttons">
                        <div class="radio-button">
                            <input id="online-<?php echo $key; ?>" type="radio" value="Онлайн" name="payment">
                            <label for="online-<?php echo $key;?>">Онлайн</label>
                        </div>
                        <div class="radio-button">
                            <input id="cash-<?php echo $key; ?>" type="radio" value="Наличные" name="payment">
                            <label for="cash-<?php echo $key; ?>">Наличные</label>
                        </div>
                    </div>
                </div>
                <div class="right-block">
                    <div class="items">
                        <div class="item">
                            <div class="image"><img class="lazy" data-src="<?php echo $image;?>" alt="#"><a data-fancybox
                                    href="<?php echo $image;?>"></a></div>
                            <div class="name">
                                <p><?php echo $product_title;?></p>
                                <div class="tags">
                                    <div class="tag color"><?php echo $color[0]['color']['name'];?></div>
                                    <div class="tag size"><?php echo $size[0]['size'];?></div>
                                </div>
                            </div>
                            <div class="price"><?php echo $size[0]['price'];?><i class="icon-ruble"></i></div>
                        </div>
                    </div>
                    <div class="total">
                        <p class="price">Итого:<strong><?php echo $size[0]['price'];?><i class="icon-ruble"></i></strong></p><button class="button"
                            href="#">Оформить заказ</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- modals sales block (b4) end -->

<!-- slider modals satrt -->
<?php foreach ($b5_slider as $key => $slideModal):?>
<?php 

$id = $slideModal["slide"]["sales_product"]->ID;
$product_title = get_the_title($id);
$color = get_field('colors', $id);
if(get_field('colors', $id)){
    $image = $color[0]['color']['photo_color_product'][0]['image'];
}
else{
   $model = get_field('model_image', $id);
   $image = $model[0]['image'];
}

$size = get_field('size_and_price', $id);


?>


<div class="modal fade checkout-modal  modal-<?php echo $key; ?>" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"><a class="close icon-remove" href="#" data-dismiss="modal" aria-hidden="true"></a>
            <form class="checkout-forms-modal">
                <div class="left-block">
                    <p>Оформление заказа<span>Настоящим подтверждаю, что я ознакомлен и согласен с условиями<a>политики конфиденциальности</a></span>
                    </p>
                    <div class="input">
                        <label>Введите имя</label>
                        <input type="text" placeholder="Иван Иванов" name="name">
                    </div>
                    <div class="input">
                        <label>Введите e-mail</label>
                        <input type="email" placeholder="example@mail.com" name="email">
                    </div>
                    <div class="input">
                        <label>Введите телефон</label>
                        <input type="tel" placeholder="+7 (495) 000 - 00 - 00" name="phone">
                    </div>
                    <div class="textarea">
                        <label>Комментарии</label>
                        <textarea placeholder="Пожелания..." name="massage"></textarea>
                    </div>
                    <div class="radio-buttons">
                        <div class="radio-button">
                            <input id="online-<?php echo $key; ?>" type="radio" value="Онлайн" name="payment">
                            <label for="online-<?php echo $key; ?>">Онлайн</label>
                        </div>
                        <div class="radio-button">
                            <input id="cash-<?php echo $key; ?>" type="radio" value="Наличные" name="payment">
                            <label for="cash-<?php echo $key; ?>">Наличные</label>
                        </div>
                    </div>
                </div>
                <div class="right-block">
                    <div class="items">
                        <div class="item">
                            <div class="image"><img class="lazy" data-src="<?php echo $image;?>" alt="#"><a data-fancybox
                                    href="<?php echo $image;?>"></a></div>
                            <div class="name">
                                <p><?php echo $product_title;?></p>
                                <div class="tags">
                                    <div class="tag color"><?php echo $color[0]['color']['name'];?></div>
                                    <div class="tag size"><?php echo $size[0]['size'];?></div>
                                </div>
                            </div>
                            <div class="price"><?php echo $size[0]['price'];?><i class="icon-ruble"></i></div>
                        </div>
                    </div>
                    <div class="total">
                        <p class="price">Итого:<strong><?php echo $size[0]['price'];?><i class="icon-ruble"></i></strong></p><button class="button"
                            href="#">Оформить заказ</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php endforeach;?>
<!-- slider modals end -->
<?php get_footer(); 
?>