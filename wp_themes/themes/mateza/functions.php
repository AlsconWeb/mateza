<?php
require get_theme_file_path('/libs/Rest_Api/rest_controller.php');
require get_theme_file_path('/libs/ajaxOrder.php');
require_once ('libs/post_type.php');

add_action( 'admin_enqueue_scripts', 'addScriptToAdmin' );
function addScriptToAdmin( $hook_suffix ){
  wp_enqueue_script( 'ajaxAdminOrder', get_template_directory_uri() .'/js/adminAjaxOrder.js', array('jquery'), '1.0', true  );
  wp_localize_script( 'ajaxAdminOrder', 'urlAjax', array(
    'url' => admin_url( 'admin-ajax.php' ),
  ));
}

add_action( 'wp_enqueue_scripts', 'theme_add_scripts' );
function theme_add_scripts() {
    //add scripts
    wp_enqueue_script( 'main-js', get_template_directory_uri() .'/js/build.js', array('jquery'), '1.0', true );
	wp_enqueue_script( 'video', get_template_directory_uri() .'/js/iphone-inline-video.browser.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'valid-js', get_template_directory_uri() .'/js/validate.js', array('main-js'), '1.0', true );
	wp_enqueue_script( 'lazy-js', get_template_directory_uri() .'/js/jquery.lazy.av.min.js', array('jquery'), '1.0', true );
	
    if(is_page_template('contact.php')){
      wp_enqueue_script( 'map-js', get_template_directory_uri() .'/js/map.js', array('Ymap-js'), '1.0', true );
      wp_enqueue_script( 'Ymap-js', 'https://api-maps.yandex.ru/2.1/?apikey=2ac3eb70-0ec0-4d7d-89d6-b7d3eabec2ec&amp;lang=ru_RU', array('main-js'), '1.0', true );
    }
    if(is_front_page(  )){
      wp_enqueue_script( 'manifest-js', get_template_directory_uri() .'/js/manifest.js', array('main-js'), '1.0', true );
      wp_enqueue_script( 'vendor-js', get_template_directory_uri() .'/js/vendor.js', array('manifest-js'), '1.0', true );
      wp_enqueue_script( 'app-js', get_template_directory_uri() .'/js/app.js', array('vendor-js'), '1.0', true );
      wp_enqueue_script( 'sweetAlert-js', 'https://cdn.jsdelivr.net/npm/sweetalert2@8', array('main-js'), '1.0', true );

      wp_enqueue_style( 'app-style', get_template_directory_uri() .'/css/app.css');
    }
    if(is_page_template('template/checkout.php') || is_page_template('template/cart.php') ){
      wp_enqueue_script( 'sweetAlert-js', 'https://cdn.jsdelivr.net/npm/sweetalert2@8', array('main-js'), '1.0', true );
    }
    
    wp_enqueue_script( 'common-js', get_template_directory_uri() .'/js/common.js', array('main-js'), '1.0', true );
   

    
    

    //add style
    wp_enqueue_style( 'main-style', get_template_directory_uri() .'/css/main.css');
    
    wp_localize_script( 'main-js', 'apiKey', array(
        'root' => esc_url_raw( rest_url()),
        'nonce' => wp_create_nonce('wp_rest')
    ));

    $id = get_page_by_path('contacts'); 
    $homeUrl = site_url();
    $posts = get_posts(array(
      'name' => 'checkout',
      'posts_per_page' => 1,
      'post_type' => 'page',
      'post_status' => 'publish'
    ));
    $url = $posts[0]->guid;

    wp_localize_script( 'main-js', 'maps_options', array(
      'imgUrl' => get_template_directory_uri(),
      'longitude' => get_field('longitude_coordinators', $id->ID),
      'latitude' => get_field('latitude_coordinates', $id->ID),
    ));

    wp_localize_script( 'main-js', 'urlAjax', array(
      'url' => admin_url( 'admin-ajax.php' ),
      'homeUrl' => get_option('home'),
      'checkoutURl' => '/checkout/',
      'cartURl' => '/cart/',
    ));
    
}

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
}

add_action( 'after_setup_theme', 'theme_register_nav_menu' );
function theme_register_nav_menu() {
	register_nav_menu( 'primary', 'Primary Menu' );
}

add_filter('upload_mimes', 'add_svg_type');
function add_svg_type($mime_types){
  $mime_types['svg'] = 'text/plain';
  
  return $mime_types;
}
?>