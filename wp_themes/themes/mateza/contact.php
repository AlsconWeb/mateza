<?php 
/**
 * Template Name: Contact
 * Author: Alex L. 
 * Author UTL: https://www.upwork.com/freelancers/~015d44c720e5f4462a
 * Author UTL: https://freelance.ru/lovik
 */
 $id = $post->ID;
 $address = get_field('address', $id);
 $address_link = get_field('address_link', $id);
 $phone_title = get_field('phone_title', $id);
 $phone_text = get_field('phone_text', $id);
 $email = get_field('e-mail', $id);
 $requisites_title = get_field('requisites_title', $id);
 $requisites = get_field('requisites', $id);
 $longitude_coordinators = get_field('longitude_coordinators', $id);
 $latitude_coordinates = get_field('latitude_coordinates', $id);


$viber = get_field('viber', 'options');
$telegram = get_field('telegram', 'options');
$whatsapp  = get_field('whatsap', 'options');
$vk = get_field('vk', 'options');
$fb = get_field('fb', 'options');
$tw = get_field('tw', 'options');
$inst = get_field('inst', 'options')

?>

<?php get_header();?>
<section>
    <div class="contacts">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h1>Контакты</h1>
                    <div class="left-block"><a href="<?= $address_link;?>"><?= $address; ?></a>
                        <ul class="tel">
                        <li><a class="icon-telegram" href="tg://resolve?domain=<?php echo $telegram;?>"></a></li>
                            <li><a class="icon-viber" href="viber://add?number=<?php echo $viber;?>"></a></li>
                            <li><a class="icon-whatsapp" href="https://api.whatsapp.com/send?phone=<?php echo $whatsapp;?>"></a></li>
                            <li><a href="tel:<?= $phone_text;?>"><span><?= $phone_title;?></span><?= $phone_text;?></a></li>
                        </ul><a href="mailto:<?= $email;?>">E-mail: <?= $email;?></a>
                        <p><?= $requisites_title; ?></p>
                        <?= $requisites;?>
                        <!-- <ul class="desc">
                            <li>ООО "Матейз"</li>
                            <li>ИНН 7728439540</li>
                            <li>КПП 772801001</li>
                            <li>ОГРН 1187746737144</li>
                            <li>Юридический адрес: Проспект Новоясеневский 32, корпус 1, офис № 110 (этаж 1, помещение VI, к)</li>
                            <li><a href="tel:88005511602">Тел. 8-800-551-16-02</a></li> 
                        </ul> -->
                    </div>
                    <div class="right-block">
                        <div id="map"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer();?>