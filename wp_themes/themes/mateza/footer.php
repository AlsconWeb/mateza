<?php
$tel = get_field('tel', 'options');
$viber = get_field('viber', 'options');
$telegram = get_field('telegram', 'options');
$whatsapp  = get_field('whatsap', 'options');
$vk = get_field('vk', 'options');
$fb = get_field('fb', 'options');
$tw = get_field('tw', 'options');
$inst = get_field('inst', 'options');
$logo_header  = get_field('logo_header', 'options');
$copyright = get_field('copyright', 'options');
if(is_front_page()){
    $id = $post->ID;
    $guarantees  = get_field('guarantees', $id);
}
?>
<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php 
                    wp_nav_menu( [
                        'theme_location'  => 'primary',
                        'menu'            => 'Menu', 
                        'container'       => '', 
                        'container_class' => '', 
                        'container_id'    => '',
                        'menu_class'      => 'menu', 
                        'menu_id'         => '',
                        'echo'            => true,
                        'fallback_cb'     => 'wp_page_menu',
                        'before'          => '',
                        'after'           => '',
                        'link_before'     => '',
                        'link_after'      => '',
                        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'depth'           => 0,
                        'walker'          => '',
                    ] );
                ?>
                <div class="left-block"><a class="logo" href="#">
                        <img class="lazy" data-src="<?php echo $logo_header['url'];?>" alt="<?php echo $logo_header['title'];?>"></a>
                    <p><img class="lazy" data-src="<?php bloginfo( 'template_url' )?>/img/rus.webp" alt="#">Russia</p>
                </div>
                <div class="right-block">
                    <ul class="tel">
                        <li><a class="icon-telegram" href="tg://resolve?domain=<?php echo $telegram;?>"></a></li>
                        <li><a class="icon-viber" href="viber://add?number=<?php echo $viber;?>"></a></li>
                        <?php 
                            $text = urlencode('text=Здравствуйте меня заинтересовал товар в вашем магазине');    
                        ?>
                        <li><a class="icon-whatsapp" href="https://api.whatsapp.com/send?phone=<?php echo $whatsapp.'&'.$text;?>"></a></li>
                        <li><a href="tel:<?php echo $tel;?>"><?php echo $tel;?></a></li>
                    </ul>
                    <ul class="soc">
                        <li><a class="icon-vk" href="<?php echo $vk;?>"></a></li>
                        <li><a class="icon-facebook" href="<?php echo $fb;?>"></a></li>
                        <li><a class="icon-twitter" href="<?php echo $tw;?>"></a></li>
                        <li><a class="icon-instagram" href="<?php echo $inst;?>"></a></li>
                    </ul>
                </div>
                <div class="copyright"><?php echo $copyright; ?></div>
            </div>
        </div>
    </div>
</footer>
<?php 
    // var_dump(is_null($_SESSION['newOrder']));

    if($_SESSION['newOrder'] && count($_SESSION['newOrder'])!=0 && is_null($_SESSION['newOrder']) != true ){ 
        $countCart =  count($_SESSION['newOrder']);
    } else { 
        $countCart = 0;
    }

    ?>
<a class="cart icon-cart" href="#"> <sup><?php  echo $countCart;?></sup></a>
<?php foreach ($guarantees as $key => $value):?>
<div class="modal modal-desc fade modal-text-<?php echo $key?>" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content"><a class="close icon-remove" href="#" data-dismiss="modal" aria-hidden="true"></a>
            <!-- <form method="post" class="forms-contact" data-init="home">
                <p>Lorem ipsum dolor.</p>
                <div class="input">
                    <label>Введите имя</label>
                    <input type="text" placeholder="Иван Иванов" name="name">
                </div>
                <div class="input">
                    <label>Введите e-mail</label>
                    <input type="email" placeholder="example@mail.com" name="email">
                </div>
                <div class="input">
                    <label>Введите телефон</label>
                    <input type="tel" placeholder="+7 (495) 000 - 00 - 00" name="phone">
                </div>
                <div class="input">
                    <input type="submit" value="Отправить">
                </div>
            </form> -->
            <div class="content-model">
                <?php echo $value['guarantee']["pop_up_text"];?>
            </div>
        </div>
    </div>
</div>
<?php endforeach;?>
<?php wp_footer();?>
</body>

</html>