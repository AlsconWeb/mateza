jQuery(document).ready(function($){

    var cart =  $('.cart sup');
    
    var totalPrice = 0;
    // var price = 0;

    if($('span.count').text() >= 2){
        $('span.count').show();
    }
    
    totalPrices();

    if(cart.text() == '0' || window.location.href !== urlAjax.homeUrl+'/'){
        $('.cart').hide();
    }
    else{
        $('.cart').show();
    }

    $('.cart sup').click(function(e){
        e.preventDefault();
        window.location.href = urlAjax.cartURl
    });

    function totalPrices(){

        
        var priceInput = $('input[name=price]');
        var url = window.location.href.split('/')
        console.log(url)
        if(url[3] == 'checkout'){
    
            
            priceInput.each(function (index, el) {
                var numCheckout = $(this).siblings('.image').find('.count').text();
                price = $(this).val().split(' ').join('');
                price = parseInt(price)
                if(numCheckout>1 || priceInput.length > 1){
                    totalPrice =  price * numCheckout + totalPrice;
                }
                else{
                    totalPrice = price
                }
                
                
            });
            $('.price #totalPrice').text(totalPrice);
        }

        if(url[3] == "cart"){
            totalPrice = 0;
            console.log('priceInput',priceInput);
            priceInput.each(function (index, el) {
                var num = $(this).siblings('.quantity').find('.input-number').val()
                price = $(this).val().split(' ').join('');
                price = parseInt(price)
                totalPrice =  price * num + totalPrice;
                
                totalItemsOrder[index].count = $(this).siblings('.quantity').find('.input-number').val()
                
            });
            $('.price strong').text(totalPrice);

        }
    }

    $('.input-number-increment').click(function (e) {
         
        totalPrices()  
    });
    $('.input-number-decrement').click(function (e) {
         
        totalPrices()  
    });


    $('.icon-remove').click(function(e){
        
        
        console.log("remove");
        var index = $(this).parents().parents('.item').data('index');
        $(this).parents().parents('.item').detach();

        var deleteItem = {
            action:"deleteItem",
            index:index,
        }
        $.ajax({
            url: urlAjax.url,
            type: "POST",
            data: deleteItem, 
            success: function(res){
                let newItem = JSON.parse(res)
                console.log("newItem", newItem);
                totalItemsOrder = newItem.newSesion;
                totalPrices();      
            }
        });
        
    });

    $('.to-checkout').click(function(e){
        e.preventDefault();
        var toCheckout = {
            action:"to-checkout",
            data:totalItemsOrder
        }
        console.log("totalItemsOrder", totalItemsOrder);
        
        $.ajax({
            url: urlAjax.url,
            type: "POST",
            data: toCheckout, 
            success: function(res){
                window.location.href = urlAjax.checkoutURl
            }
        });
        
    });

    
    $('.checkout-forms').submit(function(e){
        e.preventDefault();

        var checkoutData = {
            action:"newOrder",
            name:$('input[name=name]').val(),
            email:$('input[name=email]').val(),
            phone:$('input[name=phone]').val(),
            massage:$('textarea[name=comment]').val(),
            payment:$('input[name=payment]:checked').val(),
            totalPrice:totalPrice,
            products:totalItemsOrder
        };
        // console.log("DATA ", checkoutData);
        $.ajax({
            url: urlAjax.url,
            type: "POST",
            data: checkoutData, 
            success: function(res){
                console.log("reS", res);
                Swal.fire(
                    'Заказ принят в обработку',
                    'Спасибо. В ближайшее время наши менеджеры обязательно свяжутся с Вами',
                    'success'
                  ).then((result) => {
                      if(result){
                          window.location.href =  urlAjax.homeUrl;
                      }
                  })
                
            }
        });

    });

    $('.forms-contact').submit(function(e){
        e.preventDefault();

        var dataForm = {
            action:"sendForms",
            name:$(this).find('input[name=name]').val(),
            email:$(this).find('input[name=email]').val(),
            phone:$(this).find('input[name=phone]').val(),
            address:$(this).find('input[name=address]').val(),
            target:$(this).data('init'),
        }

        $.ajax({
            url: urlAjax.url,
            type: "POST",
            data: dataForm, 
            success: function(res){
                console.log("reS", res);
                Swal.fire(
                    'Ваша заявка была принята.',
                    'Спасибо. В ближайшее время наши менеджеры обязательно свяжутся с Вами',
                    'success'
                  ).then((result) => {
                    if(result){
                        $('.calculate').modal('hide')
                        $('.modal-form').modal('hide')
                    }
                })
                
            }
        });
    });


    $('.checkout-forms-modal').submit(function(e){
        e.preventDefault();

        var dataForms = {
            action:"formCheckoutModal",
            name:$(this).find('input[name=name]').val(),
            email:$(this).find('input[name=email]').val(),
            phone:$(this).find('input[name=phone]').val(),
            payment:$(this).find('input[name=payment]:checked').val(),
            massage:$(this).find('textarea[name=massage]').val(),
            product: {
                model: $(this).find('.right-block .items .item .name').find('p').text(),
                size: $(this).find('.right-block .items .item .name').find('.tags .size').text(),
                color: $(this).find('.right-block .items .item .name').find('.tags .color').text(),
                price: $(this).find('.right-block .items .item .price').text(),
                image: $(this).find('.right-block .items .item .image img').attr('src'),

            }
        }
        console.log('dataForms ', dataForms);

        $.ajax({
            url: urlAjax.url,
            type: "POST",
            data: dataForms, 
            success: function(res){
                console.log("reS", res);
                Swal.fire(
                    'Ваша заявка была принята.',
                    'Спасибо. В ближайшее время наши менеджеры обязательно свяжутся с Вами',
                    'success'
                  ).then((result) => {
                    if(result){
                        $('.checkout-modal').modal('hide')
                    }
                })
                
            }
        });
    });

    // $('.button[data-target=".modal-form"]').click(function(e){


    // });
	
});