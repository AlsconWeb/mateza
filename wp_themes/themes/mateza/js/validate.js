jQuery(function ($) {
	var email = $('[type="email"]'),
		name = $('[type="text"]');
	name.change(function(){
		console.log($(this).val().length)
		if($(this).val().length >= 3){
			$(this).parent().find("p").remove();
			$(this).addClass('success');
			$(this).removeClass('incorrect');
		}else{
			if($(this).parent().find('.error').length == 0){
				$(this).parent().append('<p class="error">Не меньше 3 символов...</p>')
			}
			$(this).removeClass('success');
			$(this).addClass('incorrect');
		}
	});
	email.change(function(){
		var input = this.value;
		if (input && /(^\w.*@\w+\.\w)/.test(input)) {
			$(this).parent().find("p").remove();
			$(this).addClass('success');
			$(this).removeClass('incorrect');
		} else {
			if($(this).parent().find('.error').length == 0){
				$(this).parent().append('<p class="error">Не правельный email...</p>')
			}
			$(this).removeClass('success');
			$(this).addClass('incorrect');
		}
	});

	$('[type="tel"]').on("keypress", function (e) {
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			return false;
		}
	});
	$('[type="tel"]').on("keydown", function (e) {
		if (e.which == 27) { /*escape*/
			$(this).val("").blur();
		} else if (e.which == 8) { /*backspace*/
			var digits = $(this).val();
			digits = digits.replace(/[^0-9]/g, "");
			if (digits.length == 2) {
				digits = "+" + digits.substring(1, 3);
				$(this).val(digits);
			}
		}
	})
	$('[type="tel"]').change(function(){
		var digits = $(this).val();
		if(digits.length == 16){
			$(this).parent().find("p").remove();
			$(this).addClass('success');
			$(this).removeClass('incorrect');
		}else{
			if($(this).parent().find('.error').length == 0){
				$(this).parent().append('<p class="error">Не правельный номер...</p>')
			}
			$(this).removeClass('success');
			$(this).addClass('incorrect');
		}
	});
	$('[type="tel"]').on("keyup", function () {
		var digits = $(this).val();
		digits = digits.replace(/[^0-9]/g, "");
		var len = digits.length;
		
		if (len == 0) {
			digits = "";
		} else if (len < 5) {
			digits = "+" + digits.substring(0, 1) + "(" + digits.substring(1, 4);
		} else if (len < 8) {
			digits = "+" + digits.substring(0, 1) + "(" + digits.substring(1, 4) + ") " + digits.substring(4, 7);
		} else {
			digits = "+" + digits.substring(0, 1) + "(" + digits.substring(1, 4) + ") " + digits.substring(4, 7) + "-" + digits.substring(7, 11);
		}
		$(this).val(digits);
	});
});
$('.checkout-modal').on('hidden.bs.modal', function (e) {
	$('[type="text"],[type="tel"], [type="email"], textarea').val('')
	$('[type="text"],[type="tel"], [type="email"]').removeClass('success');
	$('[type="text"],[type="tel"], [type="email"]').removeClass('incorrect');
})
$(window).load(function(){
	var video = document.querySelector('video');
	setTimeout(function () { video.play(); }, 1000);
});


