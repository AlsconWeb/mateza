<?php

add_action( 'init', 'register_post_types' );
function register_post_types(){
	register_post_type('product', array(
		'label'  => null,
		'labels' => array(
			'name'               => 'Продукты',
			'singular_name'      => 'Продукт',
			'add_new'            => 'Добавить Продукт',
			'add_new_item'       => 'Добавление Продукта',
			'edit_item'          => 'Редактирование Продукт',
			'new_item'           => 'Новое Продукт',
			'view_item'          => 'Смотреть Продукт',
			'search_items'       => 'Искать Продукт',
			'not_found'          => 'Не найдено',
			'not_found_in_trash' => 'Не найдено в корзине',
			'parent_item_colon'  => '',
			'menu_name'          => 'Продукты',
		),
		'description'         => '',
		'public'              => true,
		'show_in_rest'        => true, 
		'rest_base'           => 'product',
		'rest_controller_class' =>'',
		'menu_position'       => 3,
		'menu_icon'           => 'dashicons-cart', 
		'supports'           => array('title','editor','author','thumbnail','excerpt','revisions'),
		'taxonomies'          => array('product-cat'),
		'has_archive'         => true,
		'rewrite'             => true,
		'query_var'           => true,
		'hierarchical'       => false,
		'capability_type' => 'post'
	) );
	
	register_taxonomy('product-cat', array('product'), array(
		'label' => $labels->name,
		'labels'                => array(
			'name'              => 'Категоря товара',
			'singular_name'     => 'Категоря товара',
			'search_items'      => 'Поиск товара',
			'all_items'         => 'Все товары',
			'view_item '        => 'Просмотреть категорию',
			'parent_item'       => 'Родительский категория',
			'parent_item_colon' => 'Родительский категория',
			'edit_item'         => 'Редактировать категорию',
			'update_item'       => 'Обновить категорию',
			'add_new_item'      => 'Новая категория',
			'new_item_name'     => 'Новое имя категории',
			'menu_name'         => 'Категория товара',
		),
		'hierarchical' => true,
		'sort' => true,
		'show_admin_column' => true,
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_rest' => true,
	) );

	// register_taxonomy('collections', array('product'), array(
	// 	'label' => $labels->name,
	// 	'labels'                => array(
	// 		'name'              => 'Колекция',
	// 		'singular_name'     => 'Колекция товара',
	// 		'search_items'      => 'Колекции товара',
	// 		'all_items'         => 'Все колекции',
	// 		'view_item '        => 'Просмотреть колекцию',
	// 		'parent_item'       => 'Родительский колекцию',
	// 		'parent_item_colon' => 'Родительский колекцию',
	// 		'edit_item'         => 'Редактировать колекцию',
	// 		'update_item'       => 'Обновить колекцию',
	// 		'add_new_item'      => 'Новая колекция',
	// 		'new_item_name'     => 'Новое имя колекции',
	// 		'menu_name'         => 'Колекция',
	// 	),
	// 	'hierarchical' => true,
	// 	'sort' => true,
	// 	'show_admin_column' => true,
	// 	'public' => true,
	// 	'show_ui' => true,
	// 	'show_in_menu' => true,
	// 	'show_in_nav_menus' => true,
	// 	'show_in_rest' => true,
	// ) );

	$labels = array(	
		'name'               => 'Заказы',
		'singular_name'      => 'Заказ',
		'menu_name'          => 'Заказы',
		'name_admin_bar'     => 'Заказы',
		'edit_item'          => 'Изменить заказа',
		'view_item'          => 'Подробнее',
		'all_items'          => 'Все заказы',
		'search_items'       => 'Поиск заказа',
		'not_found'          => 'Заказ не найден.',
		'not_found_in_trash' => 'Закак не найден.',
		
	);
	$args = array(
	
		'labels'             => $labels,
		'description'        =>'Описание.',
		'menu_icon'           => 'dashicons-feedback', 
		'public'             => true,
		'publicly_queryable' => false,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'capability_type'    => 'post',
		'hierarchical'       => false,
		'menu_position'      => 2,
		'supports'           => array('title','editor','author','thumbnail','excerpt'),
		
	);
	register_post_type('orders', $args);

	add_filter('manage_orders_posts_columns', 'vr_columns');

    function vr_columns($columns) {
		
		$select = '<select>';

        $columns = array(
			'cb' => '<input type="checkbox" />',
			'image' =>"Картинка",
            'title' => 'Имя клиента',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'model' => 'Модель(ли)',
            'commentsMess' => 'Комментарии',
			'status' => 'Статус заказа',
            'priceTotal' => 'Общая сумма заказа',
        
		);
		return $columns; 
	}
	
}

add_action('manage_orders_posts_custom_column' , 'order_column_value', 10, 2);

function order_column_value($column, $post_id) {
	
	$name = get_post_meta($post_id, 'al_name', true);
	$email = get_post_meta($post_id, 'al_email', true);
	$phone = get_post_meta($post_id, 'al_phone', true);
	$model = get_post_meta($post_id, 'al_model', true);
	$color = get_post_meta($post_id, 'al_color', true);
	$message = get_post_meta($post_id, 'al_message', true);
	$totalPrice = get_post_meta($post_id, 'al_priceTotal', true);
	$image = get_post_meta($post_id, 'al_image', true);
	$status = get_post_meta($post_id, 'al_status', true);

	switch($column){
		case 'email':
			echo $email;
			break;
		case 'phone':
			echo $phone;
			break;
		case 'model':
			if(is_array($model)){
				foreach($model as  $k => $item ){
					echo $item.' '.'<b>'.$color[$k].'</b>'.'<br>';
				}
			}
			else{
				echo $model.' '.'<b>'.$color.'</b>';
			}
			break;
		case 'commentsMess':
			echo substr($message, 0, 25);
			break;
		case 'status':
			echo '<select class="order-status" data-postID="'.$post_id.'">';
			if($status == 'new'){
				echo '<option selected value="new">Новый</option>';
			}
			else {
				echo '<option value="new">Новый</option>';
			}
			if($status == 'in_processing'){
				echo '<option selected value="in_processing">В обработке</option>';
			}else{
				echo '<option value="in_processing">В обработке</option>';
			}
			if($status == 'fulfilled'){
				echo '<option selected value="fulfilled">Выполнен</option>';
			}else{
				echo '<option value="fulfilled">Выполнен</option>';
			}
			echo '</select>';
			echo '<input type="submit" value="Обновить" class="btn_update">';
			break;
		case 'image':
			echo '<img src="'.$image.'"width="128">';
			break;
		case 'priceTotal':
			echo $totalPrice . 'p.';
			break;
	}
	
}
function save_custom_taxonomy_meta( $term_id ) {
	//  die;
	if ( ! isset($_POST['extra']) ) return;
	if ( ! current_user_can('edit_term', $term_id) ) return;
	if (
		! wp_verify_nonce( $_POST['_wpnonce'], "update-tag_$term_id" ) && // wp_nonce_field( 'update-tag_' . $tag_ID );
		! wp_verify_nonce( $_POST['_wpnonce_add-tag'], "add-tag" ) // wp_nonce_field('add-tag', '_wpnonce_add-tag');
	) return;

	// Все ОК! Теперь, нужно сохранить/удалить данные
	$extra = wp_unslash($_POST['extra']);

	
	foreach( $extra as $key => $val ){
		// проверка ключа
		$_key = sanitize_key( $key );
		if( $_key !== $key ) wp_die( 'bad key'. esc_html($key) );
		
		// очистка
		if( $_key === 'tag_posts_shortcode_links' )
		$val = sanitize_textarea_field( strip_tags($val) );
		else
		$val = sanitize_text_field( $val );
		// var_dump($key, $val);
		
		// сохранение
		if( ! $val )
		delete_term_meta( $term_id, $_key );
		else
		update_term_meta( $term_id, $_key, $val );
		
	}

	return $term_id;
}
add_action( 'product-cat_add_form_fields', 'add_calass_item' );
function add_calass_item( $taxonomy ){
	
	echo '<h2>'._e('Добавьте CSS класс:').'</h2>';
	echo '<p><label from="class_name"> Введите CSS класс который будет добавлен в меню </label>';
	echo '<input id="class_name" name="extra[class]" placeholder="" value="">';
	echo '</p>';
	echo '<input type="hidden" name="extra_fields_nonce" value="'.wp_create_nonce(__FILE__).'" />';
	
}
add_action("product-cat_edit_form_fields", 'edit_product_cat_class');
function edit_product_cat_class($term){
	echo '<lable>'._e('Добавьте CSS класс: ').'</lable>';
	echo '<input id="class_name" name="extra[class]" placeholder="" value="'.esc_attr( get_term_meta( $term->term_id, 'class', 1 ) ).'">';
	echo '<p class="description"> Введите CSS класс который будет добавлен в меню </p>';
}

add_action("edited_product-cat", 'save_custom_taxonomy_meta');
add_action("create_product-cat", 'save_custom_taxonomy_meta');