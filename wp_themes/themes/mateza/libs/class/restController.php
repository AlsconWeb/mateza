<?php
 namespace INER;

class Prod_REST_Controller extends \WP_REST_Posts_Controller{
    
    public function __constructor($post_type){

        parent:: __constructor($post_type);

        $this->namespace = '/product/v1';
        $this->resource_name = 'products';

      
        
    }

    public function register_routes(){
        register_rest_route( $this->namespace, '/', $this->resource_name, array(
            array(
                'methods' => \WP_REST_Server::READABLE,
                'callback' => array($this, 'get_items'),
                // 'permission_callback' =>array($this, 'get_items_permission_check'),
            ),
            'schema' => array($this, 'get_item_schema'),
        ) );
    }

    public function get_item_schema(){
        $prod_schema = array(
            '$schema' => 'http://json-schema.org/draft-04/schema#',
            'title' => 'products',
            'type' =>'object',
            'properties' => array(
                'id' =>'ID product',
                'type' =>'integer',
                'context' => array('view'),
                'readonly' => true,
            ),
            'prodItem'=> array(
                array(
                    'title'=> array(
                        'type' =>'string',
                        'context' => array('view'),
                        'readonly' => true,
                    ),
                    'readMoreTitle' => array(
                        'type' =>'string',
                        'context' => array('view'),
                        'readonly' => true,
                        ),
                    'noteTitle' => array(
                        'type' =>'string',
                        'context' => array('view'),
                        'readonly' => true,
                        ),
                    'packageOut'  => array(
                        'type' =>'array',
                        'context' => array('view'),
                        'readonly' => true,
                        ),
                    'packageIn'  => array(
                        'type' =>'array',
                        'context' => array('view'),
                        'readonly' => true,
                        ),
                    
                )
            ),
            'description' => array(
                'type' =>'string',
                'context' => array('view'),
                'readonly' => true,
                ),
            'price' => array(
                'type' =>'integer',
                'context' => array('view'),
                'readonly' => true,
                ),
            'color' => array(
                'type' =>'array',
                'context' => array('view'),
                'readonly' => true,
                ),
            'size' => array(
                'type' =>'array',
                'context' => array('view'),
                'readonly' => true,
                ),
            'model' => array(
                'type' =>'array',
                'context' => array('view'),
                'readonly' => true,
                ),
            
        );
        
        return $prod_schema;
    }

    public function get_items($request){
        
        $data = array();

        $args = array(
            'post_type' => 'product',
            'post_per_page' => -1,
        );
        
        $query = new \WP_Query();
        $posts = $post -> query($args);

        var_dump($posts);
    }
}


?>