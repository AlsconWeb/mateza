<?php
session_start();
add_action('wp_ajax_newOrder', 'newOrder');
add_action('wp_ajax_nopriv_newOrder', 'newOrder');

function newOrder(){
    $order = $_POST;

    $name = $order['name'];
    $phone = $order['phone'];
    $email = $order['email'];
    $model =[];
    $color = [];
    $message = $order['massage'];
    $totalPrice = $order['totalPrice'];
    $image = $order['products'][0]['image'];
    $count = $order['products'];
   
    foreach($order['products'] as $models){
        $model[] = $models['model'];
    }

    foreach($order['products'] as $colors){
        $color[] = $colors['color'];
    }

    
    
    $content = '<h1>Заказ</h1>';
    $content .= '<p>Имя: '. $name .'</p>';
    $content .= '<p>Телефон: '. $phone .'</p>';
    $content .= '<p>Email: '. $email .'</p>';
    $content .= '<p>Сообщение: '. $message .'</p>';
    foreach($count as $item){
        $content .= '<p>Модель: '. $item['model'];
        $content .= ' х '. $item['count'];
        $content .=' Цвет: '. $item['color'] .'</p>';
        $content .= '<p>Размер: '.$item['size'].'</p>';
    }
    $content .='<p>Общая цена: ' . $totalPrice .'</p>';

    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

    $post_data = array(
        'post_title'    => $name,
        'post_content'  => $content,
        'post_status'   => 'publish',
        'post_author'   => 1,
        'post_type' => 'orders'
    );


    
    $post_id = wp_insert_post( $post_data );
    if( is_wp_error($post_id) ){
        $error = $post_id->get_error_message();
        die(json_encode(array('error' => $error)));
    }
    else {
        
        add_post_meta($post_id, 'al_name', $name);
        add_post_meta($post_id, 'al_phone', $phone);
        add_post_meta($post_id, 'al_email', $email);
        add_post_meta($post_id, 'al_model', $model);
        add_post_meta($post_id, 'al_color', $color);
        add_post_meta($post_id, 'al_message', $message);
        add_post_meta($post_id, 'al_priceTotal', $totalPrice);
        add_post_meta($post_id, 'al_image',  $image);
        add_post_meta($post_id, 'al_status',  'new');

        mail(get_option('admin_email'), 'Новый заказ с сайта '. get_option( 'blogname' ), $content, $headers);

        session_destroy();
        
        die(json_encode(array('status' => 'ok', "order"=> $order, "POST" => $post_id)));
    }
}
add_action('wp_ajax_update_status', 'updateStatus');
add_action('wp_ajax_nopriv_update_status', 'updateStatus'); 

function updateStatus(){
    $data = $_POST;
    $id = $_POST['data']['post_id'];
    $status = $_POST['data']['val'];
    
    update_post_meta($id, 'al_status', $status);
    

    die(json_encode(array('status' => 'ok', "data"=> $data)));
}

add_action('wp_ajax_addCard', 'addCard');
add_action('wp_ajax_nopriv_addCard', 'addCard');

function addCard(){
    session_start();
    $var = $_GET;
    $newOrd;
    $allOrd = array();
    if(isset($_SESSION['newOrder'])){
        $allOrd = $_SESSION['newOrder'];

        $allOrd[] = $var;
        $_SESSION['newOrder'] = $allOrd;
    }
    else{
        $allOrd[] = $var;
        $_SESSION['newOrder'] = $allOrd;
    }

    die(json_encode(array('status' => 'ok', "cards_item"=> $_SESSION['newOrder'])));
}

add_action('wp_ajax_goToCheckout', 'toCheckout');
add_action('wp_ajax_nopriv_goToCheckout', 'toCheckout');

function toCheckout(){
   
    $var = $_GET;
    $newOrd;
    $allOrd = array();
    $allOrd[] = $var;
    $_SESSION['newOrder'] = $allOrd;
    
    $posts = get_posts(array(
    'name' => 'checkout',
    'posts_per_page' => 1,
    'post_type' => 'page',
    'post_status' => 'publish'
    ));
    $url = $posts[0]->guid;
    
    die(json_encode(array('status' => 'ok', 'checkoutURl'=>$url, "cards_item"=> $_SESSION['newOrder'])));
}

add_action('wp_ajax_deleteItem', 'deleteItem');
add_action('wp_ajax_nopriv_deleteItem', 'deleteItem');

function deleteItem(){
    $i = $_POST["index"];
    $i = (int)$i;
    
    array_splice($_SESSION['newOrder'], $i, 1);
    
    // var_dump($_SESSION['newOrder']);

    die(json_encode(array('status' => 'ok', 'newSesion'=>$_SESSION['newOrder'])));
}

add_action('wp_ajax_to-checkout', 'toCheckoutOnCart');
add_action('wp_ajax_nopriv_to-checkout', 'toCheckoutOnCart');

function toCheckoutOnCart(){
    $data = $_POST['data'];
    $_SESSION['newOrder'] = "";
    $_SESSION['newOrder'] = $data;

    die(json_encode(array('status' => 'ok', 'newSesion'=>$_SESSION['newOrder'])));
}

add_action('wp_ajax_sendForms', 'sendForms');
add_action('wp_ajax_nopriv_sendForms', 'sendForms');

function sendForms(){
    $data = $_POST;
    $blogname = get_option( 'blogname' );

    $target = $data['target'];

    $message = "<p>Имя: ". $data['name']. '</p>';
    $message .= "<p>Телефон: ". $data['phone']. '</p>';
    $message .= "<p>E-mail: ". $data['email']. '</p>';
    $message .= "<p>Адрес: ". $data['address']. '</p>';

    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

    $subj = 'Новый заказ с сайта  '. $blogname .'  Форма:  '. $target;


    mail(get_option('admin_email'), $subj , $message, $headers);
    
}

add_action('wp_ajax_formCheckoutModal', 'newOrderModal');
add_action('wp_ajax_nopriv_formCheckoutModal', 'newOrderModal');

function newOrderModal(){
    $dataNewOrderModal = $_POST;

    $name = $dataNewOrderModal['name'];
    $phone = $dataNewOrderModal['phone'];
    $email = $dataNewOrderModal['email'];
    $model = $dataNewOrderModal['product']['model'];
    $color = $dataNewOrderModal['product']['color'];
    $size = $dataNewOrderModal['product']['size'];
    $message = $dataNewOrderModal['massage'];
    $totalPrice = $dataNewOrderModal['product']['price'];
    $image = $dataNewOrderModal['product']['image'];

    $content = '<h1>Заказ</h1>';
    $content .= '<p>Имя: '. $name .'</p>';
    $content .= '<p>Телефон: '. $phone .'</p>';
    $content .= '<p>Email: '. $email .'</p>';
    $content .= '<p>Сообщение: '. $message .'</p>';
    $content .= '<p>Модель: '. $model;
    $content .= ' х 1';
    $content .=' Цвет: '. $color .'</p>';
    $content .= '<p>Размер: '.$size.'</p>';
    $content .='<p>Общая цена: ' . $totalPrice .'</p>';

    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

    $post_data = array(
        'post_title'    => $name,
        'post_content'  => $content,
        'post_status'   => 'publish',
        'post_author'   => 1,
        'post_type' => 'orders'
    );
    
    $post_id = wp_insert_post( $post_data );
    if( is_wp_error($post_id) ){
        $error = $post_id->get_error_message();
        die(json_encode(array('error' => $error)));
    }
    else {
        
        add_post_meta($post_id, 'al_name', $name);
        add_post_meta($post_id, 'al_phone', $phone);
        add_post_meta($post_id, 'al_email', $email);
        add_post_meta($post_id, 'al_model', $model);
        add_post_meta($post_id, 'al_color', $color);
        add_post_meta($post_id, 'al_message', $message);
        add_post_meta($post_id, 'al_priceTotal', $totalPrice);
        add_post_meta($post_id, 'al_image',  $image);
        add_post_meta($post_id, 'al_status',  'new');

        mail(get_option('admin_email'), 'Новый заказ с сайта '. get_option( 'blogname' ), $content, $headers);
        
        die(json_encode(array('status' => 'ok', "POST" => $post_id)));
    }



   
    die(json_encode(array('status' => 'ok')));
}

?>