<?php
add_action( 'rest_api_init', 'rest_api_prod');

function rest_api_prod(){
    register_rest_route('rest_prod/v1', 'products', array(
        'methods' => WP_REST_SERVER::READABLE,
        'callback' => 'get_products',
    ));

    register_rest_route('rest_prod/v1', 'product-cat', array(
        'methods' => WP_REST_SERVER::READABLE,
        'callback' => 'get_products_cat',
    ));

    register_rest_route('rest_prod/v1', 'product-by-id', array(
        'methods' => WP_REST_SERVER::READABLE,
        'callback' => 'get_products_by_id',
    ));
    
}
function get_products(WP_REST_Request $request){

    
    $args = array(
        'post_type' => 'product',
        'posts_per_page' =>-1,
        'tax_query' => [
            [
                'taxonomy' => 'product-cat',
                'field'    => 'id',
                'terms'    => array($request['cat_id']),
            ]
        ]
    );
    
    $query = new WP_Query($args);
    $id_post = [];
    $product = [];
    $allProducts =[];
    
    foreach($query->posts as $ids){
        $id_post[] = $ids->ID;
        $product['models']['id'] = $ids->ID;
        $product['models']['title'] = get_the_title($ids->ID);
        $allProducts[] = $product;
    }
    wp_reset_query(  );
    return $allProducts;
}

function get_products_cat(){
    $terms = get_terms( array(
        'taxonomy'      => array( 'product-cat'), // название таксономии с WP 4.5
        'orderby'       => 'id', 
        'order'         => 'ASC',
        'hide_empty'    => false, 
        'object_ids'    => null,
        'include'       => array(),
        'exclude'       => array(), 
        'exclude_tree'  => array(), 
        'fields'        => 'all', 
        'count'         => false,
        'hierarchical'  => true, 
        'child_of'      => 0, 
        'pad_counts'    => false, 
        'cache_domain'  => 'core',
        'childless'     => false, // true не получит (пропустит) термины у которых есть дочерние термины. C 4.2.
        'update_term_meta_cache' => true, // подгружать метаданные в кэш
    ) );
    
    $cat = [];
    $allCat = [];

    foreach( $terms as $index => $term ){
        $cat['idCat'] = $term->term_id;
        $cat['title'] = $term->name;
        $cat['modal'] = false;
        $cat['className'] = get_term_meta( $term->term_id, 'class', 1 );
        if($index == 0){
            $cat['active'] = true;
        }
        else {
            $cat['active'] = false;
        }
        $cat['product_in_cat'] = $term->count;

        $allCat[] = $cat;
    }
    
    return $allCat;
}

function get_products_by_id(WP_REST_Request $request){

    $id = $request['id'];
    $post = get_post($id);
    $postsById['title'] = get_the_title($post);
    $postsById['material'] = get_field('material', $id);
    $postsById['productPackage'] = get_field('product_packages', $id);
    $postsById['description'] = get_field('description', $id);
    $postsById['video'] = get_field('video', $id);
    $postsById['color'] = get_field('colors', $id);
   
    return $postsById;
}

?>