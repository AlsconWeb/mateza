<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php bloginfo('name')?> | <?php bloginfo( 'description' )?></title>
    <?php wp_head();?>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700|Roboto:300,400,600,700" rel="stylesheet">
    <!--(if lt IE 9)-->
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <!--(endif)-->
</head>
<?php

$id = $post->ID;
// header contact options
$tel = get_field('tel', 'options');
$viber = get_field('viber', 'options');
$telegram = get_field('telegram', 'options');
$whatsapp  = get_field('whatsap', 'options');
$vk = get_field('vk', 'options');
$fb = get_field('fb', 'options');
$tw = get_field('tw', 'options');
$inst = get_field('inst', 'options');
$logo_header  = get_field('logo_header', 'options');

// main banner
$image = get_field('image', 'options');
$video = get_field('video', 'options');
$logo = get_field('logo', 'options');
$banner_title = get_field('banner_title', 'options');
$sub_title = get_field('sub_title', 'options');
session_start();
?>

<body>
    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <a class="logo" href="<?php bloginfo( 'url' )?>">
                        <img src="<?php echo $logo_header['url'];?>" alt="<?php echo $logo_header['title'];?>">
                    </a>
                    <div class="links">
                        <ul class="tel">
                            <li><a class="icon-telegram" href="tg://resolve?domain=<?php echo $telegram;?>"></a></li>
                            <li><a class="icon-viber" href="viber://add?number=<?php echo $viber;?>"></a></li>
                            <li><a class="icon-whatsapp"
                                    href="https://api.whatsapp.com/send?phone=<?php echo $whatsapp;?>"></a>
                            </li>
                            <li><a href="tel:<?php echo $tel;?>"><?php echo $tel;?></a></li>
                        </ul>
                        <div class="burger-menu"><span></span><span></span><span></span></div>
                        <?php 
							wp_nav_menu( [
								'theme_location'  => 'primary',
								'menu'            => 'Menu', 
								'container'       => '', 
								'container_class' => '', 
								'container_id'    => '',
								'menu_class'      => 'menu', 
								'menu_id'         => '',
								'echo'            => true,
								'fallback_cb'     => 'wp_page_menu',
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								'depth'           => 0,
								'walker'          => '',
							] );
						?>
                        <ul class="soc">
                            <li><a class="icon-vk" href="<?php echo $vk;?>"></a></li>
                            <li><a class="icon-facebook" href="<?php echo $fb;?>"></a></li>
                            <li><a class="icon-twitter" href="<?php echo $tw;?>"></a></li>
                            <li><a class="icon-instagram" href="<?php echo $inst;?>"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section>
        <?php if(is_front_page(  )):?>
        <div class="banner">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="video" style ="background: url(<?php echo $image['url'];?>) no-repeat center center; background-size: cover; height: 28.125vw;">
                           	<!-- <video autoplay="autoplay" loop="loop" webkit-playsinline="true" playsinline="true" muted="muted">
								<source src="<?php echo $video['url'];?>">
								 <source src="<?php bloginfo('template_url')?>/img/video.mp4">
							</video> -->
                        </div>
                        <div class="banner-content">
                            <img src="<?php echo $logo['url'];?>" alt="<?php echo $logo['title'];?>">
                            <div><?php echo $banner_title;?> <?php echo $sub_title;?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif;?>