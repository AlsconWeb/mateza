ymaps.ready(init);

function init() {
	var myMap = new ymaps.Map("map", {
		center: [55.601008, 37.608883],
		zoom: 17,
		controls: []
	}, {
		searchControlProvider: 'yandex#search'
	});
	myGeoObject = new ymaps.GeoObject({
		geometry: {
			type: "Point",
			coordinates: [55.601008, 37.608883]
		},
		properties: {
			iconContent: '<img class="map-marker" src="img/logo.png" alt="logo">',
		}
	}, {});
	myMap.behaviors
		.disable('scrollZoom')
	myMap.geoObjects
		.add(myGeoObject)
}
/*END MAP*/
