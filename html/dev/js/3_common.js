jQuery(document).ready(function($){
	/*START BURGER MENU CLICK*/
	$('.burger-menu').click(function(){
		$(this).toggleClass('close');
		$('body').toggleClass('modal-open');
	});
	/*END BURGER MENU CLICK*/
	/*START SLIDER PRODUCT*/
	$('.product-slider').slick({
		prevArrow:'<i class="icon-arrow-left">',
		nextArrow:'<i class="icon-arrow-right">',
		infinite: false,
		lazyLoad: 'ondemand',
		responsive: [
		{
			breakpoint: 767,
			settings: {
				adaptiveHeight: true
			}
		}]
	});
	/*END SLIDER PRODUCT*/
	/*START SLIDER PORTFOLIO*/
	$('.portfolio-slider').slick({
		prevArrow:'<i class="icon-arrow-left">',
		nextArrow:'<i class="icon-arrow-right">',
		infinite: false,
		speed: 300,
		slidesToShow: 3,
		responsive: [
		{
			breakpoint: 767,
			settings: {
				slidesToShow: 2,
			}
		},
		{
			breakpoint: 479,
			settings: {
				slidesToShow: 1,
			}
		}]
	});
	/*END SLIDER PORTFOLIO*/
	/*START SLIDER PORTFOLIO*/
	$('.slider-shares').slick({
		prevArrow:'<i class="icon-arrow-left">',
		nextArrow:'<i class="icon-arrow-right">',
		speed: 300,
		vertical:true,
		dots:true,
	});
	/*END SLIDER PORTFOLIO*/
	/*START SLIDER PORTFOLIO*/
	$('.reviews-slider').slick({
		prevArrow:'<i class="icon-arrow-left">',
		nextArrow:'<i class="icon-arrow-right">',
		speed: 300,
		slidesToShow: 5,
		responsive: [
		{
			breakpoint: 991,
			settings: {
				slidesToShow: 3,
			}
		},{
			breakpoint: 380,
			settings: {
				slidesToShow: 2,
			}
		}]
	});
	/*END SLIDER PORTFOLIO*/
	/*START CLICK MORE*/
	$('.more').click(function(e){
		e.preventDefault();
		$(this).toggleClass('open');
		$('.description').slideToggle();
	});
	/*END CLICK MORE*/
	/*START TAB CLICK*/
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		$('.product-slider').slick('unslick');
		$('.product-slider').slick({
			prevArrow:'<i class="icon-arrow-left">',
			nextArrow:'<i class="icon-arrow-right">',
			infinite: false,
		});
	});

	/*END TAB CLICK*/
	/*START SELECT*/
	$(".custom-select").each(function () {
		var classes = $(this).attr("class"),
			id = $(this).attr("id"),
			name = $(this).attr("name");
		var template = '<div class="' + classes + '">';
		template += '<span class="custom-select-trigger">' + $(this).attr("placeholder") + '</span>';
		template += '<div class="custom-options">';
		$(this).find("option").each(function () {
			template += '<span class="custom-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
		});
		template += '</div></div>';

		$(this).wrap('<div class="custom-select-wrapper"></div>');
		$(this).hide();
		$(this).after(template);
	});
	$(".custom-option:first-of-type").hover(function () {
		$(this).parents(".custom-options").addClass("option-hover");
	}, function () {
		$(this).parents(".custom-options").removeClass("option-hover");
	});
	$(".custom-select-trigger").on("click", function () {
		$('html').one('click', function () {
			$(".custom-select").removeClass("opened");
		});
		$(this).parents(".custom-select").toggleClass("opened");
		event.stopPropagation();
	});
	$(".custom-option").on("click", function () {
		$(this).parents(".custom-select-wrapper").find("select").val($(this).data("value"));
		$(this).parents(".custom-options").find(".custom-option").removeClass("selection");
		$(this).addClass("selection");
		$(this).parents(".custom-select").removeClass("opened");
		$(this).parents(".custom-select").find(".custom-select-trigger").text($(this).text());
	});
	/*END SELECT*/
	/*START VIDEO*/
	if($('.video').length){
		$("video").click(function() {
			if (this.paused) {
				$("video").trigger("pause");
				this.play();
				$(this)
					.parent()
					.addClass("icon-stop");
			} else {
				$("video")
					.parent()
					.addClass("icon-stop");
				$(this)
					.parent()
					.removeClass("icon-stop");
				this.pause();
			}
		});
	}
	/*END VIDEO*/

	/*START INPUT CART*/
	$(".input-number").keypress(function (event) {
		event = event || window.event;
		if (event.charCode && event.charCode != 0 && event.charCode != 46 && (event.charCode < 48 || event.charCode > 57)) {
			return false;
		}
	});
	$(".input-number").each(function () {
		var min = $(this).attr("min"),
			max = $(this).attr("max"),
			el = $(this);
		el.dec = $(this).prev();
		el.inc = $(this).next();
		el.keyup(function () {
			var value = el.val();
			if (value < 100) {
				el.css('width', '40px')
				if (value < 10) {
					el.css('width', '20px')
				}
			}
			if (value > 10) {
				el.css('width', '40px')
				if (value > 100) {
					el.css('width', '60px')
				}
			}

		});
		el.dec.mousedown(function () {
			var value = el[0].value;
			value--;
			if (!min || value >= min) {
				el[0].value = value;
			}
			if (value < 100) {
				el.css('width', '40px')
				if (value < 10) {
					el.css('width', '20px')
				}
			}

		});
		el.inc.mousedown(function () {
			var value = el[0].value;
			value++;
			if (!max || value <= max) {
				el[0].value = value++;
			}
			if (value > 10) {
				el.css('width', '40px')
				if (value > 100) {
					el.css('width', '60px')
				}
			}
		});
	});
	/*END INPUT CART*/
});
	/*START IMAGE LOAD*/
$(function() {
	$('.lazy').Lazy({
        customLoaderName: function(element) {
            element.html('element handled by "customLoaderName"');
            element.load();
        },
        asyncLoader: function(element, response) {
            setTimeout(function() {
                element.html('element handled by "asyncLoader"');
                response(true);
            }, 1000);
        }
    });
});
	/*END IMAGE LOAD*/
